
<section id="bottom">
    <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="widget">
                    <h3>IT Services-CEB</h3>
                    <ul>
                        <li><a href="#">Qui sommes-nous ?</a></li>
                        <li><a href="#">Recrutement</a></li>
                        <li><a href="#">Nos Services</a></li>
                        <li><a href="#">Nos Portofolio</a></li>
                        <li><a href="#">Nous Contacter</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-4 col-sm-6">
                <div class="widget">
                    <h3>Contact</h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-location-arrow"></i> Lot près 49 Anjanahary IIS  Antananarivo 101 Madagascar</a></li>
                        <li><a href="#"><i class="fa fa-envelope"></i> contact@itservices-ceb.com</a></li>
                        <li><a href="#"><i class="fa fa-phone"></i> +261 034 37 487 05 | +033 </a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

        

            <div class="col-md-4 col-sm-6">
                <div class="widget">
                    <h3>SUIVEZ NOUS SUR</h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook h1"></i></a> 
                        <a href="#"><i class="fa fa-twitter h1"></i></a>
                        <a href="#"><i class="fa fa-linkedin h1"></i></a>                       
                    </ul>
                </div>    
            </div><!--/.col-md-3-->
        </div>
    </div>
</section><!--/#bottom-->

<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; 2015 <a target="_blank" href="#" title="">IT Services-CEB</a>. All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <ul class="pull-right">
                    <li><a href="#">Accueil</a></li>
                    <li><a href="#">Qui sommes-nous ?</a></li>
                    <li><a href="#">Faq</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer><!--/#footer-->

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/main.js"></script>
<script src="js/wow.min.js"></script>